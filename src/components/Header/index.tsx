import React from 'react';
import logo from './BigRedBook-logo.png';
import '../../uikit.css';
import MeetingMailtoLink from "../MeetingMailtoLink";
import MeetingSwitcher from "../MeetingSwitcher";

const Header = () => (
  <header className="app-header uk-background-muted uk-flex uk-flex-column uk-flex-center uk-flex-top uk-padding">
    <div className="uk-margin-auto uk-background-secondary uk-light uk-padding uk-text-center">
      <img src={logo} className="logo uk-border-circle" alt="logo"/>
      <h1>ACA Amsterdam Zaterdag 11:00</h1>
      <h1>"Strengthening Our Recovery" Meeting</h1>
      <h3><MeetingMailtoLink/></h3>
      <h4 className="uk-text-small">Group #NET0019, Intergroup #598</h4>
      <p className="uk-text-small">Last business meeting: 3.February 2024</p>
      <MeetingSwitcher/>
    </div>
  </header>
);

Header.displayName = 'Header';

export default Header;
