import React from 'react';
import Link from "../Link";

const MeetingMailtoLink: React.FC = () => (
  <><Link href="mailto:acazaterdag@gmail.com">acazaterdag@gmail.com</Link> </>
);

MeetingMailtoLink.displayName = 'MeetingMailtoLink';

export default MeetingMailtoLink;
