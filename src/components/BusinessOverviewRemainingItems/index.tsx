import React from 'react';
import { Accordion, AccordionItem } from "uikit-react";
import BusinessMeetingItems from "../BusinessMeetingItems";

const BusinessOverviewRemainingItems = () => {
  const Content = (
    <>
      <Accordion className="items" options="multiple: true">
        <BusinessMeetingItems/>
      </Accordion>
      <p>The items on today’s business meeting agenda are: <em>[Read the list of items and record
        them in the notebook.]</em></p>
      <p>Are there any issues that we need to add to today’s agenda?</p>
      <p>Taking into consideration the items on today’s agenda, at what time shall we agree to end today’s meeting?</p>
    </>
  );

  return (
    <AccordionItem id="business-overview-remaining-items" title="Overview of Remaining Agenda Items" content={Content} />
  );
};

BusinessOverviewRemainingItems.displayName = 'BusinessOverviewRemainingItems';

export default BusinessOverviewRemainingItems;
