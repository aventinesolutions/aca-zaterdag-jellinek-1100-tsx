import React from 'react';
import { AccordionItem } from "uikit-react";

const BusinessAddressingItems = () => {
  const Content = (
    <>
      <p>According to the Big Red Book, pages 594-597, we strive to hear everyone’s reasoned
        opinion and come to consensus on each issue.  When that is not possible, we vote. We
        will begin by going around the room for each person to give ideas on a specific
        agenda item one at a time (two minutes or less each), followed by an additional
        round for responses/comments/questions (one minute or less each), followed by an official
        proposal (i.e. motion) and a vote.</p>
      <p>Now we will begin with the first item on the agenda. <em>[Go through each item until five minutes before
        the agreed-upon end time. Take notes about the results/decisions/status of each
        item in the group’s notebook.]</em>
      </p>
      <p><em>[At five minutes before the agreed-upon end time:]</em></p>
    </>
  );

  return (
    <AccordionItem title="Addressing Agenda Items" content={Content} />
  );
};

BusinessAddressingItems.displayName = 'BusinessAddressingItems';

export default BusinessAddressingItems;
