// @ts-nocheck
import React, { useContext } from 'react';
import { Button } from 'uikit-react';
import { MeetingContext } from '../../context/MeetingContext';

const MeetingSwitcher = () => {
  const [context, dispatch] = useContext(MeetingContext);
  return (
    <Button
      onClick={() => dispatch({ type: 'meetingSwitch', businessMeeting: !context.businessMeeting })}>
      switch to
      {context.businessMeeting ? ' ' : ' business '}
      meeting
    </Button>
  );
};

MeetingSwitcher.displayName = 'MeetingSwitcher';

export default MeetingSwitcher;
