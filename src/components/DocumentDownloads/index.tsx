import React from 'react';
import { AccordionItem, Card, CardBody, CardTitle, Icon } from "uikit-react";
import ButtonLink from "../ButtonLink";

const DocumentDownloads: React.FC = () => {
  const Content = (
    <>
      <Card className="uk-padding">
        <CardTitle>Meeting Format Document Downloads</CardTitle>
        <CardBody>
          <p>Face-to-face variant
            {' '}<span className="uk-text-italic">Version 21.July 2022</span></p>
          <div className="uk-margin-small">
            <ButtonLink
              id="download-format-doc"
              download
              href="./Saturday%20Meeting%20Format.doc">
              <span className="uk-text-large"><Icon options="file-text" />DOC</span></ButtonLink>
            <ButtonLink
              id="download-format-docx"
              download
              href="./Saturday%20Meeting%20Format.docx">
              <span className="uk-text-large"><Icon options="file-text" />DOCX</span></ButtonLink>
            <ButtonLink
              id="download-format-odt"
              download
              href="./Saturday%20Meeting%20Format.odt">
              <span className="uk-text-large"><Icon options="file-text" />ODT</span></ButtonLink>
            <ButtonLink
              id="download-format-pdf"
              download
              href="./Saturday%20Meeting%20Format.pdf">
              <span className="uk-text-large"><Icon options="file-text" />PDF</span></ButtonLink>
          </div>
        </CardBody>
      </Card>
      <Card className="uk-padding uk-margin-auto-top">
        <CardTitle>Business Meeting Format Document Downloads</CardTitle>
        <CardBody>
          <p className="uk-text-italic">Version 7.June 2019</p>
          <div className="uk-margin-small">
            <ButtonLink
              id="download-business-format-doc"
              download
              href="./Saturday%20Business%20Meeting%20Format.doc">
              <span className="uk-text-large"><Icon options="file-text" />DOC</span></ButtonLink>
            <ButtonLink
              id="download-business-format-docx"
              download
              href="./Saturday%20Business%20Meeting%20Format.docx">
              <span className="uk-text-large"><Icon options="file-text" />DOCX</span></ButtonLink>
            <ButtonLink
              id="download-business-format-odt"
              download
              href="./Saturday%20Business%20Meeting%20Format.odt">
              <span className="uk-text-large"><Icon options="file-text" />ODT</span></ButtonLink>
            <ButtonLink
                id="download-business-format-pdf"
                download
                href="./Saturday%20Business%20Meeting%20Format.pdf">
              <span className="uk-text-large"><Icon options="file-text" />PDF</span></ButtonLink>
          </div>
        </CardBody>
      </Card>
      <Card className="uk-padding uk-margin-auto-top">
        <CardTitle>eLiterature Documentation Downloads</CardTitle>
        <CardBody>
          <p className="uk-text-italic">Version 13.July 2022</p>
          <div className="uk-margin-small">
            <ButtonLink
                id="download-eLiterature-documentation-doc"
                download
                href="./ACA%20Amsterdam%20Zaterdag%201100%20NET0019%20eLiterature.doc">
              <span className="uk-text-large"><Icon options="file-text" />DOC</span></ButtonLink>
            <ButtonLink
                id="download-eLiterature-documentation-docx"
                download
                href="./ACA%20Amsterdam%20Zaterdag%201100%20NET0019%20eLiterature.docx">
              <span className="uk-text-large"><Icon options="file-text" />DOCX</span></ButtonLink>
            <ButtonLink
                id="download-eLiterature-documentation-odt"
                download
                href="./ACA%20Amsterdam%20Zaterdag%201100%20NET0019%20eLiterature.odt">
              <span className="uk-text-large"><Icon options="file-text" />ODT</span></ButtonLink>
            <ButtonLink
                id="download-eLiterature-documentation-pdf"
                download
                href="./ACA%20Amsterdam%20Zaterdag%201100%20NET0019%20eLiterature.pdf">
              <span className="uk-text-large"><Icon options="file-text" />PDF</span></ButtonLink>
          </div>
        </CardBody>
      </Card>
    </>
  );

  return (
    <AccordionItem id="document-downloads" title="Document Downloads" content={Content} />
  );

};

DocumentDownloads.displayName = 'DocumentDownloads';

export default DocumentDownloads;
