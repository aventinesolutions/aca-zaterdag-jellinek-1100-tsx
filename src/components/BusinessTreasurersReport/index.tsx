import React from 'react';
import { AccordionItem } from "uikit-react";

const BusinessTreasurersReport = () => {
  const Content = (
    <>
      <p>The treasurer or representative will now give the treasurer’s report, including the incoming Seventh
        Tradition amount since the last report, the expenditures since the last report, the current balance,
        the upcoming expenses, and whether or not the group is financially solvent. </p>
      <p><em>[After the treasurer’s report:]</em></p>
      <p>This group keeps a prudent reserve of three months’ rent. Are there financial issues that we need to
        add to today’s business meeting agenda?</p>
    </>
  );

  return (
    <AccordionItem title="Treasurer's Report" content={Content} />
  );
};

BusinessTreasurersReport.displayName = 'BusinessTreasurersReport';

export default BusinessTreasurersReport;
