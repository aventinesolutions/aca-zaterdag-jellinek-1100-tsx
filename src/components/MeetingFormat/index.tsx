import React from 'react';
import {AccordionItem, Card, CardBody, CardTitle} from "uikit-react";
import '../../uikit.css';
import Link from '../Link';

const MeetingFormat: React.FC = () => {
  const StrengtheningMyRecoveryLink = () => (
    <Link href="https://adultchildren.org/meditation" target="_meditation">
      Strengthening My Recovery
    </Link>
  );

  const Content = (
    <>
      <Card className="uk-padding">
        <CardTitle>For the Chair Person</CardTitle>
        <CardBody>
          <h4>First Meeting of the Month:</h4>
          <p>
            <i>Step Meeting.</i> Read from <StrengtheningMyRecoveryLink/> or
            the Big Red Book on the Step that correlates with this month.
          </p>
          <h4>Second or Fourth Meeting of the Month:</h4>
          <p>
            <i>Read from today’s excerpt in <StrengtheningMyRecoveryLink/>.</i>
            In addition, ask if anyone would like to suggest an additional
            or alternative topic. You make the choice.
          </p>
          <h4>Third Meeting of the Month: </h4>
          <p>
            <i>Tradition Meeting.</i> Read from <StrengtheningMyRecoveryLink/> or
            the Big Red Book on the Tradition that correlates with this month.
          </p>
          <h4>Fifth Meeting of the Month:</h4>
          <p>
            On the fifth meeting of the month, it is a <i>Speaker Meeting</i>. The Speaker will share for 20 minutes on
            their "experience, strength and hope" and the rest of the time is sharing from the group. If there
            is no Speaker identified, the chair can ask if anyone present would like to be the Speaker.
            If not, the group may choose a topic and identify literature to read about that topic before the sharing
            begins. For 2022, the 29th of January, the 30th of April, the 30th of July and the 29th of October will be
            fifth Saturdays of the month.
          </p>
          <h4>Readings About the Steps:</h4>
          <p>
            Readings about the steps can be found on pages 118-294 in the
            Big Red Book, and the table of contents on page V can help you
            identify the specific pages. Tradition readings can be found in
            the Big Red Book on pages 491-554. Step and tradition readings can also
            be identified in <StrengtheningMyRecoveryLink/> using the index on p. 382.
          </p>
        </CardBody>
      </Card>
      <p>
        Today is the first/second/third/fourth/fifth <em>[choose which is
        applicable]</em> meeting of the month, so it is a _____
        <em>[step, tradition, or topic]</em> meeting.
        Today’s step/tradition/reading is______, so we will begin with the
        reading on page:_____ <em>[choose relevant reading as well as the stopping
        point if reading is in Big Red Book]</em>. Who would like to begin reading?
      </p>
      <p>
        <em>For topic meetings only:</em>
        Is there anyone who would suggest an additional topic?
      </p>
      <p>
        I would now like to open the meeting for sharing. You are welcome to
        share in English or Dutch, but if there are
        fellows among us who do not speak Dutch, please share in
        English if you can.
      </p>
    </>
  );

  return (
    <AccordionItem title="Meeting Format" content={Content}/>
  );
};

MeetingFormat.displayName = 'MeetingFormat';

export default MeetingFormat;
