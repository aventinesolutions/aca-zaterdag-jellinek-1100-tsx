import React from 'react';
import { AccordionItem, Card, CardBody, CardTitle } from "uikit-react";

const BusinessPrayer = () => {
  const Content = (
    <>
      <Card className="uk-padding">
        <CardTitle>[Chair may choose a prayer or meditation. Three suggestions are:]</CardTitle>
        <CardBody>
          <h4>a. Tradition Two Meditation (BRB p. 502):</h4>
          <p className="uk-text-italic uk-text-center">Higher Power. I understand that you make your voice
          heard in a group conscience.
            <br />I ask you to remind me that the life of my program and, therefore,
          my own recovery depends upon my willingness to put the group’s welfare above my own will.
            <br />Where I disagree with the common view of my fellows in service, allow me to
            state my case honestly and respectfully.
            <br />Allow me to listen to and consider the views of others.
            <br />May I state my view and support all group decisions, including the ones I might disagree with.
            <br />Your will, not mine, be done.</p>
          <h4>b. Serenity Prayer:</h4>
          <p className="uk-text-italic uk-text-center">God, grant me the serenity tThe courage to change the one I can,o accept the things I cannot change,
            <br />The courage to change the things I can,
            <br />And the wisdom to know the difference.
          </p>
          <h4>c. ACA Serenity Prayer:</h4>
          <p className="uk-text-italic uk-text-center">God, grant me the serenity to accept the people I cannot change,
            <br />The courage to change the one I can,
            <br />And the wisdom to know that one is me.
          </p>
        </CardBody>
      </Card>
    </>
  );

  return (
    <AccordionItem title="Prayer/Meditation" content={Content} />
  );
};

BusinessPrayer.displayName = 'BusinessPrayer';

export default BusinessPrayer;
