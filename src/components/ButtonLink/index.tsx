import * as React from 'react';
import { ButtonLinkProps } from "../../global.c";

const ButtonLink: React.FC<ButtonLinkProps> = ({ id, download, style, href, target, children }) => (
  <a id={id}
     style={style}
     download={download}
     href={href}
     className="uk-button uk-button-primary uk-padding-small uk-margin-auto uk-button-text"
     target={target}>
    {children}
  </a>
);

ButtonLink.displayName = 'ButtonLink';

export default ButtonLink;
