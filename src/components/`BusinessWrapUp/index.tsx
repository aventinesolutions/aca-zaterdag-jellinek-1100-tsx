import React from 'react';
import { AccordionItem } from "uikit-react";

const BusinessWrapUp = () => {
  const Content = (
    <>
      <p>We have five minutes remaining. The decisions we have made today are: <em>[review decisions]</em>.
        The issues still on the agenda for the next business meeting are: <em>[review agenda items]</em>.</p>
      <h3 className="uk-text-center">We will now end with the Serenity Prayer:</h3>
      <p className="uk-text-italic uk-text-center">
        God, grant me the serenity to accept the things I cannot change,
        <br />The courage to change the things I can,
        <br />And the wisdom to know the difference.
      </p>
    </>
  );

  return (
    <AccordionItem title="Meeting Wrap-Up" content={Content} />
  );
};

BusinessWrapUp.displayName = 'BusinessWrapUp';

export default BusinessWrapUp;
