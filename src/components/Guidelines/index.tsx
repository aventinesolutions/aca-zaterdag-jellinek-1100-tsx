import React from 'react';
import {AccordionItem} from "uikit-react";

const Guidelines: React.FC = () => {
  const Content = (
    <>
      <p>
        We encourage each member to share openly about their experiences
        as time allows. This is a safe place to share your adult and childhood
        experiences without being judged. To allow everyone a chance to share
        during the meeting, we ask each person to limit their sharing to four
        minutes. <i>(At their discretion, the Chair Person may decide to reduce
        the sharing time to three minutes based on the number of participants.)</i>
        {' '} Is there anyone willing to be the spiritual time keeper for today?
      </p>
      <p>
        Before we begin sharing, I would like to mention that we do not "cross talk" in the meeting.
        Cross talk means interrupting, referring to, or commenting on what another person has said during the
        meeting. We do not cross talk because adult children come from family backgrounds where feelings and perceptions
        were judged as wrong or defective. In ACA, each person may share feelings and perceptions without fear of
        judgment. We accept without comment what others say because it is true for them. We work toward taking more
        responsibility in our lives rather than giving advice to others.
      </p>
      <p>
        In ACA, we do not touch, hug or attempt to comfort others when they become
        emotional during an ACA meeting. If someone begins to cry during a meeting,
        we allow the person to feel their feelings without interruption. To
        touch or hug the person is known as “fixing.” As children we tried to fix
        our parents or to control them with our behavior. In ACA we are learning to
        take care of ourselves. We support others by accepting them into our meetings
        and listening to them. We allow them to feel their feelings in peace.
      </p>
      <p>
        Some people attending ACA meetings have not grown beyond their victim or
        victimizer scripts and may attempt to meet their own needs through
        manipulation of newcomers. This is known as the “13th Step” in most Twelve
        Step programs. This violates the safety of the meeting and can drive away
        some newcomers. The love and respect we offer to newcomers is a reflection
        of the love and respect we are learning to offer ourselves.
      </p>
      <p>
        <em>A remark on therapy</em>: While many ACA members make fine use of therapists and
        counselors, our meetings are not therapy sessions. We don’t discuss therapeutic
        techniques. While we might share about our counseling work, the focus of an
        ACA meeting is the Twelve Steps and Twelve Traditions. Therapy is not
        replacement for ACA meeting or working a program.
      </p>
    </>
  );

  return (
    <AccordionItem title="Guidelines for Sharing and Interaction" content={Content}/>
  );
};

Guidelines.displayName = 'Guidelines';

export default Guidelines;
