import React from 'react';
import '../../uikit.css';
import {Accordion, AccordionItem, Card, CardBody, CardTitle} from "uikit-react";
import Link from '../Link';

const BusinessMeetingItems: React.FC = () => {
  const LocationDetails = (
    <>
      <ol>
        <li><h4>Jellinek</h4>
          <ul>
            <li><Link href="https://goo.gl/maps/JWsai1VKHmcLA6nHA" target="_maps">Jacob Obrechtstraat 92, 1071 KR Amsterdam</Link></li>
            <li>€10/meeting (same price as before)</li>
            <li>new contract; previous contract is null and void</li>
            <li>meeting coordinator must keep the COVID contact list each meeting</li>
            <li>1.5 meter distance is still required</li>
            <li>coffee and tea are free</li>
          </ul>
        </li>
        <li><h4>Trucolours</h4>
          <ul>
            <li><Link href="https://goo.gl/maps/1irEJF79e7sNpHWx6" target="_maps">Geschutswerf 12 1018 AW Amsterdam</Link></li>
            <li>€10/meeting maximum 15 people</li>
            <li>coffee and tea is €2.50 per meeting</li>
            <li>COVID contact list is optional</li>
            <li>1.5 meter distance is not required</li>
            <li>we do not have to explicitly endorse Trubendorfer</li>
          </ul>
        </li>
        <li><h4>Huis Alivia</h4>
          <ul>
            <li><Link href="https://goo.gl/maps/2RGTZfAAnFL7j6Xv7" target="_maps">Roelof Hartplein, 1071 TT Amsterdam</Link></li>
            <li>"buurtcentrum"</li>
            <li>€15/meeting from 15-20 people</li>
            <li>we get our own key to Bronckhorststraat entrance</li>
            <li>coffee and tea not yet available; but we can use the kitchen to brew our own</li>
            <li>accessibility: the stairs are steep</li>
            <li>there is a contract to sign</li>
            <li>they have a special price when we have a Business Meeting</li>
            <li>they do have a window</li>
          </ul>
        </li>
      </ol>
    </>
  );

  const Content = (
    <>
      <Card className="uk-box-shadow-hover">
        <CardTitle>Resolved Items</CardTitle>
        <CardBody>
          <ul>
            <li>We shall start meeting face-to-face Saturday the 6th of November 2021 at Trucolours
              {' '}<Link href="https://goo.gl/maps/1irEJF79e7sNpHWx6" target="_maps">Geschutswerf 12 1018 AW Amsterdam</Link>
            </li>
            <li>We shall look into the possibility of online access ("the hybrid approach") in the near future to the
              face-to-face meeting at the new location.
            </li>
            <li>The group is looking for someone to fill the Secretary position.</li>
            <li>Matthew the Treasurer contacted the previous two Treasurers and because no concrete records were kept
              by them or by Jellinek, it is currently impossible to determine if either party is owned money.
            </li>
          </ul>
        </CardBody>
      </Card>
      <Card className="uk-box-shadow-hover">
        <CardTitle>Details about the 3 possible locations that were considered</CardTitle>
        <CardBody>
          {LocationDetails}
        </CardBody>
      </Card>
      <Card className="uk-box-shadow-hover">
        <CardTitle>Open Items</CardTitle>
        <CardBody>
          <ul>
            <li>We shall continue fine tuning the meeting format starting at the third paragraph of "Guidelines for
              Sharing and Interaction"
            </li>
            <li>We shall consider additional guidelines for Newcomers, for example crosstalk and how to contact
              Fellows.
            </li>
            <li>We shall consider putting back the Intergroup minutes in the format.</li>
          </ul>
        </CardBody>
      </Card>
    </>
  );

  return (
    <AccordionItem id="business-meeting-items"
                   title="Items from the last business meeting Saturday 23rd of October 2021"
                   content={Content}/>
  )
};

BusinessMeetingItems.displayName = 'BusinessMeetingItems';

export default BusinessMeetingItems;
