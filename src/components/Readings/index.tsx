import React from 'react';
import { AccordionItem } from "uikit-react";
import Link from '../Link';

const Readings: React.FC = () => {
  const Content = (
    <>
      <p>
        We will now start with the ACA readings.
      </p>
      <ul>
        <li>Who would like to read <Link href="https://adultchildren.org/literature/problem" target="_TheProblem">The
          Problem</Link>?
          (or the <Link href="https://adultchildren.org/literature/laundry-list" target="_TheLaundryList">The Laundry
            List or The Other Laundry List</Link>)?
        </li>
        <li>Who would like to read <Link href="https://adultchildren.org/literature/solution" target="_TheSolution">The
          Solution</Link>?
        </li>
        <li>Who would like to read <Link href="https://adultchildren.org/literature/steps" target="_The12Steps">The
          Twelve Steps</Link>?
        </li>
        <li>Who would like to read <Link href="https://adultchildren.org/literature/traditions"
                                         target="_The12Traditions">The Twelve Traditions</Link>?
        </li>
      </ul>
      <p>
        You may have related to our readings even if there was no apparent
        alcoholism or addiction in your home. This is common because dysfunction
        can occur in a family without the presence of addiction. We welcome you.
      </p>
      <p>
        The ACA program is not easy, but if you can handle what comes up at six
        consecutive meetings you will start to come out of denial. Confronting
        your denial about family addiction or dysfunction will give you freedom from the
        past. Your life will change. You will truly learn how to live with
        greater choice and personal freedom. You will learn to focus on yourself
        and let others be responsible for their own lives.
      </p>
      <p>
        In the beginning, many of us could not recognize or accept that
        some of our attitudes or behaviors result from
        being raised in an alcoholic or other dysfunctional family.
        We behave as adult children, which means we bring
        self-doubt and fear learned in childhood to our adult
        interactions. By attending six meetings in a row and
        attending regularly thereafter, we come to know and begin to
        act as our True Selves.
      </p>
    </>
  );

  return (
    <AccordionItem title="Readings" content={Content} />
  );
};

Readings.displayName = 'Readings';

export default Readings;
