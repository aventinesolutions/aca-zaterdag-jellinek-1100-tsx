import React from 'react';
import {AccordionItem} from "uikit-react";
import Link from '../Link';

const Introduction = () => {
  const Content = (
    <>
      <p>
        Hello, my name is ____________. I am an adult child. Welcome to the online
        Amsterdam Saturday Morning Meeting of Adult Children of Alcoholics. We meet to share the
        experiences we had as children growing up in an alcoholic or dysfunctional home. That
        experience infected us then, and it affects us today. By practicing the Twelve Steps and by
        attending meetings regularly, we find freedom from the effects of alcoholism or other family
        dysfunction. As ACA members we identify with The Laundry List
        traits. We learn to live in The Solution by reparenting ourselves, one day at a time.
      </p>
      <p>
        In this meeting the use of the camera is optional. However, for some of us seeing each
        other is important. We ask you to take this into consideration.
        Please, when you are not sharing or taking part in the group readings or prayers,
        turn your microphone off by muting it. If you experience sound feedback or poor quality
        sound, try listening on headphones.
      </p>
      <p>
        Will you please join me in a moment of silence followed by the Serenity Prayer to open the meeting?
        (Everyone may turn on their microphones.)
      </p>
      <h4 className="uk-text-italic uk-text-center">(moment of silence)</h4>
      <p className="uk-text-italic uk-text-center">
        God, grant me the serenity to accept the things I cannot change,
        <br/>The courage to change the things I can,
        <br/>And the wisdom to know the difference.
      </p>
      <p>
        May we each turn our microphones on and introduce ourselves one by one?
      </p>
      <p>
        Do we have any newcomers today? <em>[Welcome!]</em> Will you please introduce yourself by turning on your
        microphone and giving us your first name? We are glad you are here. Please feel free to ask any questions after
        the meeting, ask for phone numbers from others and you can also read
        the <Link href="https://adultchildren.org/newcomer"
                  target="_NewcomerWelcomePage">ACA Newcomer welcome page</Link>.
      </p>
    </>
  );

  return (
    <AccordionItem title="Introduction" content={Content}/>
  );
};

Introduction.displayName = 'Introduction';

export default Introduction;
