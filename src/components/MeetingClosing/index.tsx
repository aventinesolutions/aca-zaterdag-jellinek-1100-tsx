import React from 'react';
import {Accordion, AccordionItem} from "uikit-react";
import '../../uikit.css';
import Link from '../Link';

import BusinessMeetingItems from "../BusinessMeetingItems";
import MeetingMailtoLink from "../MeetingMailtoLink";

const MeetingClosing: React.FC = () => {
  const Content = (
    <>
      <h4><em>(around 11:55)</em></h4>
      <p>
        That’s all the time we have for sharing. If you were not called on to share,
        please speak to someone after the meeting if you need to talk. What you hear
        at this meeting should remain at the meeting. We do not discuss whom we saw
        at the meeting or talk about another person’s story or experiences to other
        people. Please respect the anonymity of those who have shared here today. Who
        you see here, what you hear here, when you leave here, let it stay here.</p>
      <p>
        On the first Saturday of the month, there is a Business Meeting following the
        regular meeting. All are welcome and encouraged to attend. I, as the chairperson,
        will now read announcements from the last business meeting and existing agenda
        items of the next business meeting.</p>
      <p>
        <em>Reminder:</em> to join our online meetings, before Saturday at 10:30am CET during the week you want to join,
        send a request by email to <MeetingMailtoLink/>. Please be aware you will not receive a response before
        Saturday morning, when our meeting host will send a link to that week's meeting by 10:45am.
      </p>
      <p>Are there any other ACA related announcements?</p>
      <p>
        Who would like to read <Link href=" https://adultchildren.org/literature/promises"
                                     target="_The12Promises">The Twelve Promises
        of ACA</Link>?
      </p>
      <p>
        We will now end with a closing prayer by turning on your microphones and
        reciting the ACA Serenity Prayer together:
      </p>
      <h4 className="uk-text-italic uk-text-center">ACA Version of the Serenity Prayer</h4>
      <p className="uk-text-italic uk-text-center">
        God, grant me the serenity to accept the people I cannot change,
        <br/>The courage to change the one I can,
        <br/>And the wisdom to know that one is me.
      </p>
    </>
  );

  return (
    <AccordionItem id="meeting-closing" title="Meeting Closing" content={Content}/>
  )
};

MeetingClosing.displayName = 'MeetingClosing';

export default MeetingClosing;
