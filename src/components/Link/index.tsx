import * as React from 'react';
import { LinkWithTargetProps } from "../../global.c";

const Link: React.FC<LinkWithTargetProps> = ({ id, style, href, target, children }) => (
  <a id={id} style={style} href={href} className="uk-link-muted uk-text-primary" target={target}>
    {children}
  </a>
);

Link.displayName = 'Link';

export default Link;
