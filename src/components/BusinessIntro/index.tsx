import React from 'react';
import { AccordionItem } from "uikit-react";

const BusinessIntro = () => {
  const Content = (
    <>
      <p>[The secretary chairs the business meeting and either takes minutes or asks a fellow to take minutes.]</p>
      <p>Hello, my name is ____________. On the first Saturday of the month, the Amsterdam Saturday Morning
        Meeting of Adult Children of Alcoholics has a business meeting, which we will begin now with a
        prayer/meditation.
      </p>
    </>
  );

  return (
    <AccordionItem title="Introduction" content={Content} />
  );
};

BusinessIntro.displayName = 'BusinessIntro';

export default BusinessIntro;
