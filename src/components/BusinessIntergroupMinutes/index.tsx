import React from 'react';
import { AccordionItem } from "uikit-react";

const BusinessIntergroupMinutes = () => {
  const Content = (
    <>
      <p>The intergroup (598) representative will now report on intergroup issues.</p>
      <p><em>[After the intergroup representative’s report:]</em></p>
      <p>Are there intergroup issues that we need to add to today’s business meeting agenda?</p>
    </>
  );

  return (
    <AccordionItem id="business-intergroup-minutes" title="Intergroup #598 Minutes" content={Content} />
  );
};

BusinessIntergroupMinutes.displayName = 'BusinessIntergroupMinutes';

export default BusinessIntergroupMinutes;
