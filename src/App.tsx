// @ts-nocheck
import React, { useContext } from 'react';
import './App.css';
import './styles.scss';
import { MeetingContext, MeetingContextProvider } from './context/MeetingContext';
import { Accordion, Flex } from 'uikit-react';
import Header from './components/Header';
import Introduction from './components/Introduction';
import Readings from "./components/Readings";
import Guidelines from "./components/Guidelines";
import MeetingFormat from "./components/MeetingFormat";
import MeetingClosing from "./components/MeetingClosing";
import DocumentDownloads from "./components/DocumentDownloads";
import BusinessIntro from "./components/BusinessIntro";
import BusinessPrayer from "./components/BusinessPrayer";
import BusinessTreasurersReport from "./components/BusinessTreasurersReport";
import BusinessIntergroupMinutes from "./components/BusinessIntergroupMinutes";
import BusinessOverviewRemainingItems from "./components/BusinessOverviewRemainingItems";
import BusinessAddressingItems from "./components/BusinessAddressingItems";
import BusinessWrapUp from "./components/`BusinessWrapUp";

const UIkit = require('uikit/dist/js/uikit.js');
const Icons = require('uikit/dist/js/uikit-icons');

UIkit.use(Icons);

const MeetingContent = () => {
  const [context] = useContext(MeetingContext);

  return (
    <>
      {context.businessMeeting ?
        <>
          <h1>Business Meeting</h1>
          <Accordion options="multiple: true">
            <BusinessIntro />
            <BusinessPrayer />
            <BusinessTreasurersReport />
            <BusinessIntergroupMinutes />
            <BusinessOverviewRemainingItems />
            <BusinessAddressingItems />
            <BusinessWrapUp />
            <DocumentDownloads />
          </Accordion>
        </>
        :
        <Accordion options="multiple: true;">
          <Introduction />
          <Readings />
          <Guidelines />
          <MeetingFormat />
          <MeetingClosing />
        </Accordion>
      }
    </>
  );
};

function App() {

  return (
    <div className="App">
      <MeetingContextProvider>
        <Flex
          className="meeting-format uk-margin-auto uk-padding-small uk-width-3-5@l uk-width-auto@s uk-background-default"
          direction="column" alignment="center">
          <Header />
          <MeetingContent />
        </Flex>
      </MeetingContextProvider>
    </div>
  );
}

export default App;
