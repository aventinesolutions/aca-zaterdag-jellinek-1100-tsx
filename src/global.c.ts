export interface BaseProps {
  id?: string
  className?: string
  style?: object
}

export interface ButtonLinkProps extends BaseProps {
  href?: string
  target?: string
  download?: boolean
}

export interface LinkWithTargetProps extends BaseProps {
  type?: 'muted' | 'text' | 'heading' | 'reset'
  href?: string
  toggleOptions?: string
  target?: string
}
