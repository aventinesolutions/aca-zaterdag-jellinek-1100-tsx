// @ts-nocheck
import React, { createContext } from 'react';
import useAsyncReducer from '../hooks/useAsyncReducer';
export const MeetingContext = createContext(undefined);

const initialState = {
  businessMeeting: false,
};

const reducer = (state: any, action: any) => {
  return 'meetingSwitch' === action.type ?
    { ...state, businessMeeting: action.businessMeeting } : state;
};

export const MeetingContextProvider = (props: React.PropsWithChildren<any>) => {
  return (
    <MeetingContext.Provider value={useAsyncReducer(reducer, initialState)}>
      {props.children}
    </MeetingContext.Provider>
  );
};
